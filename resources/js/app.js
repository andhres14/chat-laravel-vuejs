/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.moment = require('moment');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('message', require('./components/Message.vue').default);
Vue.component('sent-message', require('./components/Sent.vue').default);


/*
var pusher = new Pusher('63eaf31073d549c05619', {
    cluster: 'eu',
    forceTLS: true,
    encrypted: true,
    authEndpoint : 'http://laravel-chat.local/public/broadcasting/auth',
    auth: {
        headers: {
            // I assume you have meta named `csrf-token`.
            'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]')
        }
    }
});

var channel = pusher.subscribe('private-channel');

channel.bind('MessageSentEvent', function(data) {
    var e = JSON.stringify(data);
    console.log(e);
    app.messages.push({
        message: e.message.message,
        user: e.user
    });
});*/

const app = new Vue({
    el: '#app',
    data: {
        messages: []
    },
    mounted(){
        this.fetchMessages();
        Echo.private('chat')
            .listen('MessageSentEvent', (e) => {
                console.log(e);
                this.messages.push({
                    message: e.message.message,
                    user: e.user
                })
            })
    },
    methods: {
        addMessage(message) {
            this.messages.push(message)
            axios.post('http://laravel-chat.local/public/messages', message).then(response => {
                console.log(response)
            })
        },
        fetchMessages() {
            axios.get('http://laravel-chat.local/public/messages').then(response => {
                this.messages = response.data
            })
        }
    }

});
